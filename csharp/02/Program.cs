﻿using Gtk;
using Cairo;

class CairoSample : DrawingArea {
	static void Main () {
		Application.Init ();
		Window win = new Window ("Cairo with Gtk# 3");
		win.SetDefaultSize (400, 400);
		win.DeleteEvent += delegate { Application.Quit (); };
		win.Add (new CairoSample ());
		win.ShowAll ();
		Application.Run (); }

	void OvalPath (Context cr, double xc, double yc, double xr, double yr) {
		Matrix m = cr.Matrix;
		cr.Translate (xc, yc);
		cr.Scale (1.0, yr / xr);
		cr.MoveTo (xr, 0.0);
		cr.Arc (0, 0, xr, 0, 2 * Math.PI);
		cr.ClosePath ();
		cr.Matrix = m; }
	
	void Draw3Circles (Context cr, int xc, int yc, double radius, double alpha) {
		double subradius = radius * (2 / 3.0 - 0.1);
		cr.SetSourceRGBA (1.0, 0.0, 0.0, alpha);
		OvalPath (cr, xc + radius / 3.0 * Math.Cos (Math.PI * 0.5), yc - radius / 3.0 * Math.Sin (Math.PI * 0.5), subradius, subradius);
		cr.Fill (); }

	void Draw (Context cr, int width, int height) {
		double radius = 0.5 * Math.Min (width, height) - 10;
		int xc = width / 2;
		int yc = height / 2;
		Surface overlay, punch, circles;
		using (var target = cr.GetTarget ()) {
			overlay = target.CreateSimilar (Content.ColorAlpha, width, height);
			punch   = target.CreateSimilar (Content.Alpha, width, height);
			circles = target.CreateSimilar (Content.ColorAlpha, width, height); }
		cr.Save ();
		using (Context cr_overlay = new Context (overlay)) {
			cr_overlay.SetSourceRGB (0.0, 0.0, 0.0);
			OvalPath (cr_overlay, xc, yc, radius, radius);
			cr_overlay.Fill ();
		using (Context cr_tmp = new Context (punch))
				Draw3Circles (cr_tmp, xc, yc, radius, 1.0);
			cr_overlay.Operator = Operator.DestOut;
			cr_overlay.SetSourceSurface (punch, 0, 0);
			cr_overlay.Paint ();
			using (Context cr_circles = new Context (circles)) {
				cr_circles.Operator = Operator.Over;
				Draw3Circles (cr_circles, xc, yc, radius, 0.5); }
			cr_overlay.Operator = Operator.Add;
			cr_overlay.SetSourceSurface (circles, 0, 0);
			cr_overlay.Paint (); }
		cr.SetSourceSurface (overlay, 0, 0);
		cr.Paint ();
		overlay.Dispose ();
		punch.Dispose ();
		circles.Dispose (); }

	protected override bool OnDrawn (Cairo.Context ctx) {
		Draw (ctx, AllocatedWidth, AllocatedHeight);
		return true; } }
		