﻿using System;

public class Person {
    public string Name = "";
    public void PrintName() {
        Console.WriteLine("Hi i am " + Name); } }

class Program {
    static int Main(string[] args) {
        Person person = new Person();
        person.Name = "John";
        person.PrintName();
        return 0; } }