defmodule User do
   defstruct name: "" , age: 0
end

defmodule Test do
  @moduledoc """
  Documentation for `Test`.
  """
  @doc """
  Hello world.
  ## Examples
      iex> Test.hello()
      :world
  """

  alias UUID
  alias Arrays
  
  @y "Hello World"
  def hello do
    Test.main()
  end

  def sum_and_average(x) do
	sum=Enum.sum(x)
	avg=sum/Enum.count(x)
	{sum,avg}
  end

  def print_numbers(x) do
    x |> Enum.join(" ") |> IO.puts()
  end

  def main do
    x=UUID.uuid4()
    z=:hello
    IO.puts(x)
    IO.puts(@y)
    IO.puts(z)
    s="Hi"
    if s === "Hi" do
		IO.puts("Equal of #{s}")
	end
	IO.puts(Enum.random([:gold,:silver,:bronze]))
	case s do
	  "Hello" -> IO.puts("Hello")
	  "Hi"    -> IO.puts("Hi")
	  _       -> IO.puts("Other")
	end
	IO.puts(Integer.gcd(25,10))
	t={10,"ten"}
	t=Tuple.append(t,"aaa")
	IO.inspect(t)
	prices={5,10,15}
	_avg=Tuple.sum(prices)/tuple_size(prices)
	_first=elem(prices,0)
	{_first2,_,_}=prices
	alist=[1,2,3]
	Enum.each(alist, fn(x) -> IO.puts(x) end)
	amap = %{:a => 1, 2 => :b}
	IO.puts(amap[2])
	astruct=%User{name: "Alain", age: 20}
	IO.puts(" #{astruct.name} #{astruct.age}")
	_s2=" Alain" |> String.trim()
	_u=:rand.uniform(10)
	i=String.to_integer("123")
	i="123" |> Integer.parse()
	xxx={"aaa","bbb"}
	case xxx do
	  {"aaa",_} -> IO.puts("aaa")
	  {_,_}     -> IO.puts("other")
	end
	alist=[2,3,8,9]
	for x <- alist , do: IO.puts(x)
	alist2 = for x <- alist , do:  x*x
	IO.inspect(alist2)
	alist = alist ++ [11]
	alist = alist ++ [12,15]
	alist = [ 1 | alist ]
	IO.inspect(alist)
	even= for x<- alist , rem(x,2)=== 0 , do: x
	IO.inspect(even)
	num=[1,2,3,4,5]
	Enum.each(num,fn x -> IO.puts(x) end)
	num=["1","2","3","4","5"]
	num2=Enum.map(num,&String.to_integer/1)
	IO.inspect(num2)
	num=[1,2,3,4,5]
    r=sum_and_average(num)
    {_sum,_avg}=r
    IO.inspect(r)
    print_numbers(num)
    s=" 1 2 3 4 5 " |> String.trim()
    String.split(s," ") |> Enum.map(&String.to_integer/1) |> IO.inspect()
    Process.sleep(100)
    a=[1, 2, 3] |> Enum.into(Arrays.new())
  end


end
