#Factorial implementation using standard recursion
defmodule Factorial do
	def of(0), do: 1
	def of(n) when n > 0, do: n * of(n - 1)
end

defmodule Listhelper do
	def sum([]), do: 0
	def sum([head | tail]), do: head+sum(tail)
end

defmodule Mycond do
  def mycond(c) do
		case c do
			"True" -> IO.puts("true")
			"False" -> IO.puts("false")
			true -> IO.puts("other")
		end
	end
end

defmodule Fraction do
	defstruct a: nil, b: nil
	end

	defmodule MyMmain do

  #This is a comment
	@moduledoc "My Module"
	@pi 3.14

	@doc "Compute"
	@spec area(number) ::number
	def area(r) , do: r*r*@pi

	person={"Alain",25}
	age=elem(person,1)
	person=put_elem(person,1,26)

	l=[1,2,3,4]
	le=length(l)
	e=Enum.at(l,3)
	l=List.replace_at(l,0,10)
	l=List.insert_at(l,0,11)
	l=[1,2,3]++[4,5]
	l=[ 1 | [ 2 | []]]
	h=hd(l)
	t=tl(l)

	m=%{"Alain"=>1,"Eddy"=>2}
	m1=m["Alain"]
  m2=Map.get(m,2)
  m=Map.put(m,"Jan",4)

  square=fn x -> x*x end
	x=square.(5)

	print_element=fn x -> IO.puts(x) end
	Enum.each([1,2,3],print_element)
  Enum.each([1,2,3],&IO.puts/1)
	r=1..10
	b=2 in r
	kl=[{:monday ,1},{:thuesday,2}]
	x=Keyword.get(kl,:monday)
	b= 3===3

	unless 1===2 do
		IO.puts("Hi")
	else
		IO.puts("There")
	end

	if 1===2 do
		IO.puts("Hi")
	else
		IO.puts("There")
	end

	%{age: age}=%{name: "bob",age:  25}

	x=Factorial.of(10)

	x=Listhelper.sum([1,2,3])

	Mycond.mycond("True")

	Enum.each([1,2,3],fn x -> IO.puts(x) end)

	x==Enum.map([1,2,3],fn x -> 2*x end)

	x=Enum.filter([1,2,3],fn x -> rem(x,2)==1 end)

	x=for x <-[1,2,3] do x*x end

	z=%Fraction{a: 1,b: 2}
  IO.puts(z.a)

	run_query = fn query_def ->
									Process.sleep(1000)
									"#{query_def} result"
							end
	IO.puts(run_query.("query 1"))
	Enum.map(1..5,
	        fn index ->
						query_def="query #{index}"
						IO.puts(run_query.(query_def))
	        end
	)
	async_query =
		fn query_def ->
			spawn(fn ->
							query_result=run_query.(query_def)
							IO.puts(query_result)
						end)
		end
	async_query.("Query 1")
	Process.sleep(1000)
	Enum.each(1..5,&async_query.("Query #{&1}"))
  Process.sleep(2000)
end
