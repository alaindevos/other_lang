using Plots
x = range(0, 10, length=100)
y1 = sin.(x)
y2 = cos.(x)
p=plot(x, [y1,y2],title="Tutorial",xlabel="x",ylabel="y")
savefig(p,"p.png")
display(p)
sleep(10)
