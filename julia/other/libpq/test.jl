using LibPQ
using Tables
conn = LibPQ.Connection( "host=127.0.0.1 port=5432 dbname=syslogng user=x password=x")
select_sql = "select datetime,message FROM messages_freebsd_20240308"
res=LibPQ.execute(conn,select_sql)
# Converting results to row tables
data = rowtable(res)
for d in data
    println(" | $(d[:datetime]) | $(d[:message]) | ")
end
