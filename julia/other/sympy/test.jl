using PyCall
sympy = pyimport("sympy")  #
x = sympy.Symbol("x")      # PyObject x
y = sympy.sin(x)           # PyObject sin(x)
z = y.subs(x, sympy.pi/3.0)    # PyObject 0
z2=convert(Float64, z)        # 0.0
println(z2)
