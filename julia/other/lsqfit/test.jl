using LsqFit
# The model function is: m(t,p)=p1exp(−p2t)
model(t, p) = p[1] * exp.(-p[2] * t)
# t: array of independent variable
# p: array of model parameters
# tdata: data of independent variable
# ydata: data of dependent variable
tdata=range(0,stop=10,length=20)
ydata = model(tdata, [1.0 2.0]) + 0.01*randn(length(tdata))
# initial value of parameters for curve_fit().
p0 = [0.5, 0.5]
fit = curve_fit(model, tdata, ydata, p0)
fit = curve_fit(model, tdata, ydata, p0)
param = fit.param
println(param)
