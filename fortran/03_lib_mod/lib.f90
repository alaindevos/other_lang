module lib
contains
    subroutine print_int(i)
        implicit none
        integer, intent (in) :: i
        print *, i
    end subroutine print_int

    integer function square(i) bind(C)
        implicit none
        integer,intent(in)::i
        square=i*i
    end function square
    
    FUNCTION foobar(x, y) BIND (C)
        USE ISO_C_BINDING
        IMPLICIT NONE
        !GCC$ ATTRIBUTES CDECL :: foobar
        REAL (C_DOUBLE), VALUE :: x, y
        REAL (C_DOUBLE) :: foobar
        foobar = x * y + x / y
    END
    
    
end module lib
