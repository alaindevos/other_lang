program hello
    ! A comment
    implicit none
    integer :: i
    integer :: j
    real :: r
    character :: c
    character(len=4) :: s
    logical :: b
    integer :: sarr(10)
    integer,allocatable :: darr(:)
    allocate(darr(10))
    deallocate(darr)
    i=10
    r=3.1415
    c='A'
    b=.false.
    s='ABCD'
    block
        print *, 'Inner'
    end block
    print *, 'Hello World!'
    if (0.0 < 1.0) then
        print * , 'smaller'
    else
        print * , 'bigger'
    end if
    do i=1,5
        print *,i
    end do
    i=1
    do while (i <= 5)
        print *,i
        i=i+1
    end do
    i=1
    do while (1==1)
        if (i>5) then
            exit
        end if
        print *,i
        i=i+1
    end do
    outer: do i = 1,3
        inner: do j=1,3
            print *, i+j
        end do inner
    end do outer



end program hello
