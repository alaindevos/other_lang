subroutine print_int(i)
    implicit none
    integer, intent (in) :: i
    print *, i
end subroutine print_int

function square(i) result(j)
    implicit none
    integer,intent(in)::i
    integer,intent(out)::j 
    j=i*i
end function square

program hello
    ! A comment
    implicit none
    integer :: i
    call print_int(5)
    print *, square(5)
end program hello
