const std = @import("std");

fn printit(s: []const u8) !void {
    std.debug.print("{s} \n", .{s});
}

pub fn main() !void {
    try printit("Hello World");
}
