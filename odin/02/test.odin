package main

import fmt "core:fmt"
import os "core:os"

main :: proc() {
	fmt.println("Hellope!")
	x:int=123
	f:f64=123.0
	x=234
	y::456
	s1:string="Hallo"
	i:=123
	//Fixed Array
	arr:=[5]int{1,2,3,4,5}
	for i in 0..<len(arr) {
		fmt.println(arr[i])
	}
	//Slice
	arr2:[]int=arr[2:4]
	//Dynamic array
	d:[dynamic]int
	append(&d,123)
	//string
	s:string="HelloAlain"
	sub:=s[0:5]
	//if
	if x<64 {
		fmt.println("To small")
	}
	else if x> 256 {
		fmt.println("To large")
	}
	else {
		fmt.println("OK")
	}
	//for
	for tel:=0;tel<5;tel=tel+1 {
		fmt.println(tel)
	}
	//while
	tel:=0
	for tel<5 {
		fmt.println(tel)
		tel=tel+1
	}
	for i in arr {
		fmt.println(i)
	}
	//switch
	switch 2 {
		case 1:
			fmt.println("1")
		case 2:
			fmt.println("2")
		case:
			fmt.println("other")
	}
	//struct
	Vector2::struct{
		x:f32,
		y:f32 
	}
	v:=Vector2{1.0,2.0}
	v.x=3.4;
	//Union
	U::union { i32,f32}
	u:U
	u=1
	switch uvalue in u {
		case i32: fmt.println("It's an int")
		case f32: fmt.println("It's a float")
	}
	//Enum
	Direction::enum{North,South}
	dir:=Direction.North
	//Hashmap
	m:=make(map[string]int)
	defer delete(m)
	m["Alain"]=2
	m["Alain"]=3
	fmt.println(m["Alain"])
	elem,ok:=m["Alain"]
	//Procedure
	add_mul :: proc(x:int,y:int)->(int,int){
		return x+y,x*y
	}

}
